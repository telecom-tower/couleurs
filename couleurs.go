package couleurs

import "image/color"

// Map is the main color map
var Map = map[string]color.RGBA{
	"abricot":            color.RGBA{0xe6, 0x7e, 0x30, 0xff},
	"acajou":             color.RGBA{0x88, 0x42, 0x1d, 0xff},
	"aigue-marine":       color.RGBA{0x79, 0xf8, 0xf8, 0xff},
	"alezan":             color.RGBA{0xa7, 0x67, 0x26, 0xff},
	"amande":             color.RGBA{0x82, 0xc4, 0x6c, 0xff},
	"amarante":           color.RGBA{0x91, 0x28, 0x3b, 0xff},
	"ambre":              color.RGBA{0xf0, 0xc3, 0x0, 0xff},
	"améthyste":          color.RGBA{0x88, 0x4d, 0xa7, 0xff},
	"anthracite":         color.RGBA{0x30, 0x30, 0x30, 0xff},
	"aquilain":           color.RGBA{0xad, 0x4f, 0x9, 0xff},
	"ardoise":            color.RGBA{0x5a, 0x5e, 0x6b, 0xff},
	"aubergine":          color.RGBA{0x37, 0x0, 0x28, 0xff},
	"auburn":             color.RGBA{0x9d, 0x3e, 0xc, 0xff},
	"aurore":             color.RGBA{0xff, 0xcb, 0x60, 0xff},
	"avocat":             color.RGBA{0x56, 0x82, 0x3, 0xff},
	"azur":               color.RGBA{0x0, 0x7f, 0xff, 0xff},
	"baillet":            color.RGBA{0xae, 0x64, 0x2d, 0xff},
	"basané":             color.RGBA{0x8b, 0x6c, 0x42, 0xff},
	"beige":              color.RGBA{0xc8, 0xad, 0x7f, 0xff},
	"beurre":             color.RGBA{0xf0, 0xe3, 0x6b, 0xff},
	"bis":                color.RGBA{0x76, 0x6f, 0x64, 0xff},
	"bisque":             color.RGBA{0xff, 0xe4, 0xc4, 0xff},
	"bistre":             color.RGBA{0x85, 0x6d, 0x4d, 0xff},
	"bitume":             color.RGBA{0x4e, 0x3d, 0x28, 0xff},
	"blanc cassé":        color.RGBA{0xfe, 0xfe, 0xe2, 0xff},
	"blanc lunaire":      color.RGBA{0xf4, 0xfe, 0xfe, 0xff},
	"blanc":              color.RGBA{0xff, 0xff, 0xff, 0xff},
	"bleu acier":         color.RGBA{0x3a, 0x8e, 0xba, 0xff},
	"bleu barbeau":       color.RGBA{0x54, 0x72, 0xae, 0xff},
	"bleu canard":        color.RGBA{0x4, 0x8b, 0x9a, 0xff},
	"bleu charrette":     color.RGBA{0x8e, 0xa2, 0xc6, 0xff},
	"bleu ciel":          color.RGBA{0x77, 0xb5, 0xfe, 0xff},
	"bleu céleste":       color.RGBA{0x26, 0xc4, 0xec, 0xff},
	"bleu de cobalt":     color.RGBA{0x22, 0x42, 0x7c, 0xff},
	"bleu de prusse":     color.RGBA{0x24, 0x44, 0x5c, 0xff},
	"bleu givré":         color.RGBA{0x80, 0xd0, 0xd0, 0xff},
	"bleu klein":         color.RGBA{0x0, 0x2f, 0xa7, 0xff},
	"bleu majorelle":     color.RGBA{0x60, 0x50, 0xdc, 0xff},
	"bleu marine":        color.RGBA{0x3, 0x22, 0x4c, 0xff},
	"bleu nuit":          color.RGBA{0xf, 0x5, 0x6b, 0xff},
	"bleu outremer":      color.RGBA{0x1b, 0x1, 0x9b, 0xff},
	"bleu paon":          color.RGBA{0x6, 0x77, 0x90, 0xff},
	"bleu persan":        color.RGBA{0x66, 0x0, 0xff, 0xff},
	"bleu pétrole":       color.RGBA{0x1d, 0x48, 0x51, 0xff},
	"bleu roi":           color.RGBA{0x31, 0x8c, 0xe7, 0xff},
	"bleu turquin":       color.RGBA{0x42, 0x5b, 0x8a, 0xff},
	"bleu électrique":    color.RGBA{0x2c, 0x75, 0xff, 0xff},
	"bleu":               color.RGBA{0x0, 0x80, 0xff, 0xff},
	"blond":              color.RGBA{0xe2, 0xbc, 0x74, 0xff},
	"blond vénitien":     color.RGBA{0xe7, 0xa8, 0x54, 0xff},
	"blé":                color.RGBA{0xe8, 0xd6, 0x30, 0xff},
	"bordeaux":           color.RGBA{0x6d, 0x7, 0x1a, 0xff},
	"bouton d'or":        color.RGBA{0xfc, 0xdc, 0x12, 0xff},
	"brique":             color.RGBA{0x84, 0x2e, 0x1b, 0xff},
	"bronze":             color.RGBA{0x61, 0x4e, 0x1a, 0xff},
	"brou de noix":       color.RGBA{0x3f, 0x22, 0x4, 0xff},
	"brun":               color.RGBA{0x5b, 0x3c, 0x11, 0xff},
	"caca d'oie":         color.RGBA{0xcd, 0xcd, 0xd, 0xff},
	"cacao":              color.RGBA{0x61, 0x4b, 0x3a, 0xff},
	"cachou":             color.RGBA{0x2f, 0x1b, 0xc, 0xff},
	"café au lait":       color.RGBA{0x78, 0x5e, 0x2f, 0xff},
	"café":               color.RGBA{0x46, 0x2e, 0x1, 0xff},
	"cannelle":           color.RGBA{0x7e, 0x58, 0x35, 0xff},
	"capucine":           color.RGBA{0xff, 0x5e, 0x4d, 0xff},
	"caramel":            color.RGBA{0x7e, 0x33, 0x0, 0xff},
	"carmin":             color.RGBA{0x96, 0x0, 0x18, 0xff},
	"carotte":            color.RGBA{0xf4, 0x66, 0x1b, 0xff},
	"chamois":            color.RGBA{0xd0, 0xc0, 0x7a, 0xff},
	"chartreuse":         color.RGBA{0x7f, 0xff, 0x0, 0xff},
	"chaudron":           color.RGBA{0x85, 0x53, 0xf, 0xff},
	"chocolat":           color.RGBA{0x5a, 0x3a, 0x22, 0xff},
	"châtain":            color.RGBA{0x8b, 0x6c, 0x42, 0xff},
	"cinabre":            color.RGBA{0xdb, 0x17, 0x2, 0xff},
	"citrouille":         color.RGBA{0xdf, 0x6d, 0x14, 0xff},
	"coquille d'œuf":     color.RGBA{0xfd, 0xe9, 0xe0, 0xff},
	"corail":             color.RGBA{0xe7, 0x3e, 0x1, 0xff},
	"cramoisi":           color.RGBA{0xdc, 0x14, 0x3c, 0xff},
	"crème":              color.RGBA{0xfd, 0xf1, 0xb8, 0xff},
	"cuisse de nymphe":   color.RGBA{0xfe, 0xe7, 0xf0, 0xff},
	"cuivre":             color.RGBA{0xb3, 0x67, 0x0, 0xff},
	"cyan":               color.RGBA{0x2b, 0xfa, 0xfa, 0xff},
	"cæruleum":           color.RGBA{0x35, 0x7a, 0xb7, 0xff},
	"écarlate":           color.RGBA{0xed, 0x0, 0x0, 0xff},
	"écru":               color.RGBA{0xfe, 0xfe, 0xe0, 0xff},
	"émeraude":           color.RGBA{0x1, 0xd7, 0x58, 0xff},
	"fauve":              color.RGBA{0xad, 0x4f, 0x9, 0xff},
	"flave":              color.RGBA{0xe6, 0xe6, 0x97, 0xff},
	"fraise écrasée":     color.RGBA{0xa4, 0x24, 0x24, 0xff},
	"fraise":             color.RGBA{0xbf, 0x30, 0x30, 0xff},
	"framboise":          color.RGBA{0xc7, 0x2c, 0x48, 0xff},
	"fuchsia":            color.RGBA{0xfd, 0x3f, 0x92, 0xff},
	"fumée":              color.RGBA{0xbb, 0xd2, 0xe1, 0xff},
	"garance":            color.RGBA{0xee, 0x10, 0x10, 0xff},
	"glauque":            color.RGBA{0x64, 0x9b, 0x88, 0xff},
	"glycine":            color.RGBA{0xc9, 0xa0, 0xdc, 0xff},
	"grenadine":          color.RGBA{0xe9, 0x38, 0x3f, 0xff},
	"grenat":             color.RGBA{0x6e, 0xb, 0x14, 0xff},
	"gris acier":         color.RGBA{0xaf, 0xaf, 0xaf, 0xff},
	"gris de payne":      color.RGBA{0x67, 0x71, 0x79, 0xff},
	"gris fer":           color.RGBA{0x7f, 0x7f, 0x7f, 0xff},
	"gris perle":         color.RGBA{0xce, 0xce, 0xce, 0xff},
	"gris":               color.RGBA{0x60, 0x60, 0x60, 0xff},
	"groseille":          color.RGBA{0xcf, 0xa, 0x1d, 0xff},
	"grège":              color.RGBA{0xbb, 0xae, 0x98, 0xff},
	"gueules":            color.RGBA{0xe2, 0x13, 0x13, 0xff},
	"héliotrope":         color.RGBA{0xdf, 0x73, 0xff, 0xff},
	"incarnat":           color.RGBA{0xff, 0x6f, 0x7d, 0xff},
	"indigo":             color.RGBA{0x2e, 0x0, 0x6c, 0xff},
	"isabelle":           color.RGBA{0x78, 0x5e, 0x2f, 0xff},
	"ivoire":             color.RGBA{0xff, 0xff, 0xd4, 0xff},
	"jaune canari":       color.RGBA{0xe7, 0xf0, 0xd, 0xff},
	"jaune citron":       color.RGBA{0xf7, 0xff, 0x3c, 0xff},
	"jaune d'or":         color.RGBA{0xef, 0xd8, 0x7, 0xff},
	"jaune de cobalt":    color.RGBA{0xfd, 0xee, 0x0, 0xff},
	"jaune de mars":      color.RGBA{0xee, 0xd1, 0x53, 0xff},
	"jaune de naples":    color.RGBA{0xff, 0xf0, 0xbc, 0xff},
	"jaune impérial":     color.RGBA{0xff, 0xe4, 0x36, 0xff},
	"jaune mimosa":       color.RGBA{0xfe, 0xf8, 0x6c, 0xff},
	"jaune":              color.RGBA{0xff, 0xff, 0x0, 0xff},
	"kaki":               color.RGBA{0x94, 0x81, 0x2b, 0xff},
	"lapis-lazuli":       color.RGBA{0x26, 0x61, 0x9c, 0xff},
	"lavallière":         color.RGBA{0x8f, 0x59, 0x22, 0xff},
	"lavande":            color.RGBA{0x96, 0x83, 0xec, 0xff},
	"lie de vin":         color.RGBA{0xac, 0x1e, 0x44, 0xff},
	"lilas":              color.RGBA{0xb6, 0x66, 0xd2, 0xff},
	"lime":               color.RGBA{0x9e, 0xfd, 0x38, 0xff},
	"lin":                color.RGBA{0xfa, 0xf0, 0xe6, 0xff},
	"magenta":            color.RGBA{0xdb, 0x0, 0x73, 0xff},
	"malachite":          color.RGBA{0x1f, 0xa0, 0x55, 0xff},
	"mandarine":          color.RGBA{0xfe, 0xa3, 0x47, 0xff},
	"marron":             color.RGBA{0x58, 0x29, 0x0, 0xff},
	"mastic":             color.RGBA{0xb3, 0xb1, 0x91, 0xff},
	"mauve":              color.RGBA{0xd4, 0x73, 0xd4, 0xff},
	"maïs":               color.RGBA{0xff, 0xde, 0x75, 0xff},
	"menthe":             color.RGBA{0x16, 0xb8, 0x4e, 0xff},
	"moutarde":           color.RGBA{0xc7, 0xcf, 0x0, 0xff},
	"nacarat":            color.RGBA{0xfc, 0x5d, 0x5d, 0xff},
	"nankin":             color.RGBA{0xf7, 0xe2, 0x69, 0xff},
	"noir":               color.RGBA{0x0, 0x0, 0x0, 0xff},
	"noisette":           color.RGBA{0x95, 0x56, 0x28, 0xff},
	"ocre jaune":         color.RGBA{0xdf, 0xaf, 0x2c, 0xff},
	"ocre rouge":         color.RGBA{0xdd, 0x98, 0x5c, 0xff},
	"olive":              color.RGBA{0x70, 0x8d, 0x23, 0xff},
	"or":                 color.RGBA{0xff, 0xd7, 0x0, 0xff},
	"orange brûlé":       color.RGBA{0xcc, 0x55, 0x0, 0xff},
	"orange":             color.RGBA{0xff, 0x7f, 0x0, 0xff},
	"orchidée":           color.RGBA{0xda, 0x70, 0xd6, 0xff},
	"orpiment":           color.RGBA{0xfc, 0xd2, 0x1c, 0xff},
	"paille":             color.RGBA{0xfe, 0xe3, 0x47, 0xff},
	"parme":              color.RGBA{0xcf, 0xa0, 0xe9, 0xff},
	"pelure d'oignon":    color.RGBA{0xd5, 0x84, 0x90, 0xff},
	"pervenche":          color.RGBA{0xcc, 0xcc, 0xff, 0xff},
	"pistache":           color.RGBA{0xbe, 0xf5, 0x74, 0xff},
	"poil de chameau":    color.RGBA{0xb6, 0x78, 0x23, 0xff},
	"ponceau":            color.RGBA{0xc6, 0x8, 0x0, 0xff},
	"pourpre":            color.RGBA{0x9e, 0xe, 0x40, 0xff},
	"prasin":             color.RGBA{0x4c, 0xa6, 0x6b, 0xff},
	"prune":              color.RGBA{0x81, 0x14, 0x53, 0xff},
	"puce":               color.RGBA{0x4e, 0x16, 0x9, 0xff},
	"rose mountbatten":   color.RGBA{0x99, 0x7a, 0x8d, 0xff},
	"rose":               color.RGBA{0xfd, 0x6c, 0x9e, 0xff},
	"rouge anglais":      color.RGBA{0xf7, 0x23, 0xc, 0xff},
	"rouge cardinal":     color.RGBA{0xb8, 0x20, 0x10, 0xff},
	"rouge cerise":       color.RGBA{0xbb, 0xb, 0xb, 0xff},
	"rouge d'andrinople": color.RGBA{0xa9, 0x11, 0x1, 0xff},
	"rouge de falun":     color.RGBA{0x80, 0x18, 0x18, 0xff},
	"rouge feu":          color.RGBA{0xff, 0x49, 0x1, 0xff},
	"rouge sang":       color.RGBA{0x85, 0x6, 0x6, 0xff},
	"rouge tomette":      color.RGBA{0xae, 0x4a, 0x34, 0xff},
	"rouge":              color.RGBA{0xf0, 0x0, 0x20, 0xff},
	"rouille":            color.RGBA{0x98, 0x57, 0x17, 0xff},
	"roux":               color.RGBA{0xad, 0x4f, 0x9, 0xff},
	"rubis":              color.RGBA{0xe0, 0x11, 0x5f, 0xff},
	"sable":              color.RGBA{0xe0, 0xcd, 0xa9, 0xff},
	"safre":              color.RGBA{0x1, 0x31, 0xb4, 0xff},
	"sang de bœuf":       color.RGBA{0x73, 0x8, 0x0, 0xff},
	"sanguine":           color.RGBA{0x85, 0x6, 0x6, 0xff},
	"saphir":             color.RGBA{0x1, 0x31, 0xb4, 0xff},
	"saumon":             color.RGBA{0xf8, 0x8e, 0x55, 0xff},
	"sinople":            color.RGBA{0x14, 0x94, 0x14, 0xff},
	"smalt":              color.RGBA{0x0, 0x33, 0x99, 0xff},
	"smaragdin":          color.RGBA{0x1, 0xd7, 0x58, 0xff},
	"soufre":             color.RGBA{0xff, 0xff, 0x6b, 0xff},
	"souris":             color.RGBA{0x9e, 0x9e, 0x9e, 0xff},
	"sépia":              color.RGBA{0xae, 0x89, 0x64, 0xff},
	"tabac":              color.RGBA{0x9f, 0x55, 0x1e, 0xff},
	"taupe":              color.RGBA{0x46, 0x3f, 0x32, 0xff},
	"terre d'ombre":      color.RGBA{0x92, 0x6d, 0x27, 0xff},
	"tomate":             color.RGBA{0xde, 0x29, 0x16, 0xff},
	"topaze":             color.RGBA{0xfa, 0xea, 0x73, 0xff},
	"tourterelle":        color.RGBA{0xbb, 0xac, 0xac, 0xff},
	"turquoise":          color.RGBA{0x25, 0xfd, 0xe9, 0xff},
	"vanille":            color.RGBA{0xe1, 0xce, 0x9a, 0xff},
	"vermeil":            color.RGBA{0xff, 0x9, 0x21, 0xff},
	"vermillon":          color.RGBA{0xdb, 0x17, 0x2, 0xff},
	"vert bouteille":     color.RGBA{0x9, 0x6a, 0x9, 0xff},
	"vert céladon":       color.RGBA{0x83, 0xa6, 0x97, 0xff},
	"vert d'eau":         color.RGBA{0xb0, 0xf2, 0xb6, 0xff},
	"vert de chrome":     color.RGBA{0x18, 0x39, 0x1e, 0xff},
	"vert de hooker":     color.RGBA{0x1b, 0x4f, 0x8, 0xff},
	"vert de vessie":     color.RGBA{0x22, 0x78, 0xf, 0xff},
	"vert impérial":      color.RGBA{0x0, 0x56, 0x1b, 0xff},
	"vert lichen":        color.RGBA{0x85, 0xc1, 0x7e, 0xff},
	"vert perroquet":     color.RGBA{0x3a, 0xf2, 0x4b, 0xff},
	"vert poireau":       color.RGBA{0x4c, 0xa6, 0x6b, 0xff},
	"vert pomme":         color.RGBA{0x34, 0xc9, 0x24, 0xff},
	"vert prairie":       color.RGBA{0x57, 0xd5, 0x3b, 0xff},
	"vert printemps":     color.RGBA{0x0, 0xff, 0x7f, 0xff},
	"vert sapin":         color.RGBA{0x9, 0x52, 0x28, 0xff},
	"vert sauge":         color.RGBA{0x68, 0x9d, 0x71, 0xff},
	"vert tilleul":       color.RGBA{0xa5, 0xd1, 0x52, 0xff},
	"vert véronèse":      color.RGBA{0x5a, 0x65, 0x21, 0xff},
	"vert épinard":       color.RGBA{0x17, 0x57, 0x32, 0xff},
	"vert":               color.RGBA{0x0, 0x80, 0x20, 0xff},
	"vert-de-gris":       color.RGBA{0x95, 0xa5, 0x95, 0xff},
	"violet d'évêque":    color.RGBA{0x72, 0x3e, 0x64, 0xff},
	"violet":             color.RGBA{0x7f, 0x0, 0xff, 0xff},
	"viride":             color.RGBA{0x40, 0x82, 0x6d, 0xff},
	"zinzolin":           color.RGBA{0x6c, 0x2, 0x77, 0xff},
}
